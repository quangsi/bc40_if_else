const LUONG_MOI_GIO = 10;

function tinhLuong() {
  var tongGioLam = document.getElementById("txt-gio-lam").value * 1;
  var tienLuong;
  if (tongGioLam < 40) {
    // th2
    tienLuong = tongGioLam * LUONG_MOI_GIO;
  } else {
    tienLuong = 40 * LUONG_MOI_GIO + (tongGioLam - 40) * 1.5 * LUONG_MOI_GIO;
    // th1
  }
  document.getElementById(
    "result"
  ).innerHTML = ` Tiền lương của bạn là : ${tienLuong}  $`;
}

// th1 : 45h 10$

// th2 : 36h 10$
